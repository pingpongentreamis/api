

var env = process.env.NODE_ENV || 'production';
var config = require('../config')[env];
const mysql = require('mysql2');

var pool = mysql.createPool({
    connectionLimit: 100, //important
    host: config.database.host,
    port: config.database.port,
    user: config.database.user,
    password: config.database.password,
    database: config.database.db,
    debug: false
});

module.exports.pool = pool
