//copy this file to config.js and change user and password fields
var config = {
production: {
    database: {
        host: 'mysql.montpellier.epsi.fr',
        port: '5206',
        user: '******',
        password: '*****',
        db:   'pingpong'
    },
    server: {
        host:   '127.0.0.1',
        port:   '443'
    },
    keys:{
        arbitreKey: "thisisthearbitrekey",
        organisationKey: "thisistheorganisationkey"
    }
}
};
module.exports = config;
