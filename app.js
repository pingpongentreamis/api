
var express = require('express');
var https = require('https');
var fs = require('fs');
var env = process.env.NODE_ENV || 'production';
var config = require('./config')[env];
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser')
var express = require('express')
var cors = require('cors')

require('events').EventEmitter.defaultMaxListeners = 200;


var hostname = config.server.host;
var port = config.server.port;


var app = express();
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
var whitelist = ['http://localhost:4200', 'http://localhost:4201', 'http://localhost:4202']
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS ' + origin))
    }
  },
  credentials: true,
  exposedHeaders: ['Content-Length', 'X-Foo', 'X-Bar'],
  method:['GET', 'PUT', 'POST', 'DELETE']
}

app.use(cors(corsOptions))


https.createServer({
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
}, app).listen(port, function () {
  console.log("serveur running on https://" + hostname + ":" + port + "\n");
});





/*app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.header("Access-Control-Allow-Headers",
    "Origin, X-Requeted-With, Content-Type, Accept, Authorization, RBR");
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});*/
require('./routes')(app);