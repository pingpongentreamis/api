module.exports = function (app) {
    var mysql = require('../services/db').pool;
    const jwt = require('jsonwebtoken');
    var env = process.env.NODE_ENV || 'production';
    var config = require('../config')[env];

    app.get('/joueurs', function (req, res) {
        mysql.query("select * from Joueur", function (err, rows) {
            if (!err) {
                res.json(rows);
            } else {
                res.json({ "code": 500, "status": err });
                return;
            }

        });
    });


    app.get('/joueur/:id', function (req, res) {
        mysql.query("select * from Joueur where id=" + req.params.id + ";", function (err, rows) {
            if (!err) {
                res.json(rows);
            } else {
                res.json({ "code": 500, "status": err });
                return;
            }

        });
    });

    app.post('/joueur', function (req, res, err) {
        console.log(req.body.Nom);
        var cookie = req.cookies.jwtTokenOrga;
        console.log("Cookie yes " + req)
        jwt.verify(cookie, config.keys.organisationKey, (err, authData) => {
            if (err) {
                res.sendStatus(403);
            } else {
                mysql.query("INSERT INTO Joueur(nom, prenom, nationalite, datenai) VALUES ('" + req.body.Nom + "','" + req.body.Prenom + "','" + req.body.Nationalite + "','" + req.body.Date_naissance + "');",
                    function (err, rows) {
                        console.log("here")
                        if (!err) {
                            res.json({ "code": 200 });
                        } else {
                            res.json({ "code": 500, "status": err });
                            return;
                        }
                        mysql.on('error', function (err) {
                            res.json({ "code": 100, "status": "Error in connection database" });

                            return;
                        });
                    });
            }
        });
    });

    app.put('/joueur/:id', (req, res, err) => {
        var cookie = req.cookies.jwtTokenOrga;
        console.log("Cookie yes " + cookie)
        jwt.verify(cookie, config.keys.organisationKey, (err, authData) => {
            if (err) {
                res.sendStatus(403);
            } else {
                mysql.query("update Joueur  set nom ='" + req.body.nom_joueur + "', prenom ='" + req.body.prenom_joueur + "', nationalite = '" + req.body.nationalite_joueur + "', datenai='" + req.body.date_naissance + "' where id =" + req.params.id + ";",
                    function (err, rows) {
                        if (!err) {
                            res.json({ "code": 200 });
                        } else {
                            res.json({ "code": 500, "status": err });
                            return;
                        }
                    });
                mysql.on('error', function (err) {
                    res.json({ "code": 100, "status": "Error in connection database" });

                    return;
                });
            }
        });
    });

    app.delete('/joueur/:id', (req, res, err) => {
        mysql.query("update Joueur set nom ='unknown', prenom ='unknown', nationalite ='unknown' where id =" + req.params.id + ";",
            function (err, rows) {
                if (!err) {
                    res.json({ "code": 200 });
                } else {
                    res.json({ "code": 500, "status": err });
                    return;
                }
            });
        mysql.on('error', function (err) {
            res.json({ "code": 100, "status": "Error in connection database" });

            return;
        });
    });
}

