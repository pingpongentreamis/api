module.exports = function (app) {
    var mysql = require('../services/db').pool;
    var bcrypt = require('bcryptjs');
    const jwt = require('jsonwebtoken');
    var env = process.env.NODE_ENV || 'production';
    var config = require('../config')[env];

    app.get('/Points', function (req, res) {
        mysql.query("select * from Point", function (err, rows) {
            if (!err) {
                res.json(rows);
            } else {
                res.json({ "code": 500, "status": err });
                return;
            }
            mysql.on('error', function (err) {
                res.json({ "code": 100, "status": "Error in connection database" });

                return;
            });
        });
    });


    app.get('/Point/:PartieId/:JoueurId', function (req, res) {
        mysql.query("select * from Point where Joueur_ID='" + req.params.JoueurId + "'AND Partie_ID='" + req.params.PartieId + "';", function (err, rows) {
            if (!err) {
                res.json(rows[0]);
            } else {
                res.json({ "code": 500, "status": err });
                return;
            }
            mysql.on('error', function (err) {
                res.json({ "code": 100, "status": "Error in connection database" });

                return;
            });
        });
    });

    app.get('/PointsByJoueurByPartie/:PartieId/:JoueurId', function (req, res) {
        var s = "select count(*) as totalPoints from Point where Joueur_ID='" + req.params.JoueurId + "'AND Partie_ID='" + req.params.PartieId + "';"
       // console.log(s)
        mysql.query(s, function (err, rows) {
            if (!err) {
                res.json(rows[0]);
            } else {
                res.json({ "code": 500, "status": err });
                return;
            }
             
        });
    });

    app.get('/Point/:PartieId', function (req, res) {
        mysql.query("select * from Point where Partie_ID=" + req.params.PartieId + " ORDER BY DateTime ASC;", function (err, rows) {
            if (!err) {
                res.json(rows);
            } else {
                res.json({ "code": 500, "status": err });
                return;
            }
            mysql.on('error', function (err) {
                res.json({ "code": 100, "status": "Error in connection database" });

                return;
            });
        });
    });

    app.post('/Point', function (req, res, err) {
        var cookie = req.cookies.jwtToken;
        console.log("Cookie yes " + req)
        jwt.verify(cookie, config.keys.arbitreKey, (err, authData) => {
            if (err) {
                res.sendStatus(403);
            } else {
                var lol = "INSERT INTO Point(Partie_Id, Joueur_Id, DateTime) VALUES ('" + req.body.PartieId + "','" + req.body.JoueurId + "','" + new Date().toISOString().slice(0, 19).replace('T', ' ')
                    + "');"
                console.log(lol)
                mysql.query(lol, function (err, rows) {
                    if (!err) {
                        res.json({ "code": 200 });
                    } else {
                        res.json({ "code": 500, "status": err });
                        return;
                    }
                    mysql.on('error', function (err) {
                        res.json({ "code": 100, "status": "Error in connection database" });
                        return;
                    });
                });
            }
        })
    });


    app.delete('/Point/:partieId/:JoueurId', function (req, res, err) {
        var cookie = req.cookies.jwtToken;
        console.log("Cookie yes " + cookie)
        jwt.verify(cookie, config.keys.arbitreKey, (err, authData) => {
            if (err) {
                res.sendStatus(403);
            } else {
                var lol = "SELECT DATE_FORMAT(DateTime, '%Y-%m-%d %H:%i:%s') as DateTime, Joueur_id, partie_Id " +
                    " FROM Point where partie_Id=" + req.params.partieId + " AND Joueur_id=" + req.params.JoueurId + " ORDER BY DateTime DESC LIMIT 1;"
                console.log(lol);
                mysql.query(lol, function (err, rows) {
                    if (!err) {
                        console.log(rows.length)
                        if (rows.length != 0) {
                            var sql = "delete from Point where partie_Id=" + req.params.partieId + " AND Joueur_id=" + req.params.JoueurId + " AND DateTime ='" + rows[0].DateTime + "';";
                            console.log(sql);
                            mysql.query(sql, function (err, rows) {
                                if (!err) {
                                    res.json({ "code": 200 });
                                } else {
                                    res.json({ "code": 500, "status": err });
                                    return;
                                }
                            });
                        }
                        else {
                            res.json({ "code": 404, "status": "not found" });
                            return;
                        }
                    }
                    else {
                        res.json({ "code": 500, "status": err });
                        return;
                    }
                    mysql.on('error', function (err) {
                        res.json({ "code": 100, "status": "Error in connection database" });
                        return;
                    });
                });
            }

        });
    })

}

