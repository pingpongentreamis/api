module.exports = function (app) {
    var mysql = require('../services/db').pool;
    var bcrypt = require('bcryptjs');
    const request = require('request');
    const jwt = require('jsonwebtoken');
    var env = process.env.NODE_ENV || 'production';
    var config = require('../config')[env];


    app.get('/Parties', function (req, res1) {
        process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
        var o = {}
        var arr = new Array();
        var step = 'one';
        finalJson(function (arr) {
            arr.sort(function (a, b) {
                return parseFloat(a.id) - parseFloat(b.id);
            });
            res1.send(arr)
        })
        function finalJson(callback) {
            mysql.query("select id, OrganisationID, Arbitre_ID, Joueur1_ID, Joueur2_ID, SportId, DateTime, etat from Partie", async (err, rows) => {
                if (!err) {
                    rows.forEach(element => {
                        build_all(element, function (arr) {
                            if (arr.length === rows.length) {
                                callback(arr)
                            }
                        });

                    });
                } else {
                    res1.json({ "code": 500, "status": err });
                    return;
                }
                mysql.on('error', function (err) {
                    res.json({ "code": 100, "status": "Error in connection database" });

                    return;
                });
            });

        }

        function build_j(j_id, callback) {
            request('https://localhost/joueur/' + j_id, { json: true }, (err, res, body) => {
                if (err) reject(err);
                callback(body[0]);
            });
        };

        function build_orga(orga_id, callback) {
            request('https://localhost/organisation/' + orga_id, { json: true }, (err, res, body) => {
                if (err) reject(err);
                callback(body[0]);
            });
        };

        function build_sport(sport_id, callback) {
            request('https://localhost/sport/' + sport_id, { json: true }, (err, res, body) => {
                if (err) reject(err);
                callback(body[0])
            });
        }

        function build_arbitre(arbitre_id, callback) {
            request('https://localhost/arbitre/' + arbitre_id, { json: true }, (err, res, body) => {
                if (err) reject(err);
                callback(body[0])
            });
        }

        function build_score(partie_id, joueur_id, callback) {
            request('https://localhost/PointsByJoueurByPartie/' + partie_id + '/' + joueur_id, { json: true }, (err, res, body) => {
                if (err) reject(err);
                callback(body)
            });
        }

        function build_all(element, callback) {
            build_j(element.Joueur1_ID, function (j1) {
                var post_obj = {};
                post_obj.id = element.id
                post_obj.etat = element.etat
                post_obj.DateTime = element.DateTime
                post_obj.joueur1 = j1
                build_j(element.Joueur2_ID, function (j2) {
                    post_obj.joueur2 = j2
                    build_arbitre(element.Arbitre_ID, function (arbitre) {
                        post_obj.arbitre = arbitre
                        build_orga(element.OrganisationID, function (orga) {
                            post_obj.organisation = orga
                            build_sport(element.SportId, function (sport) {
                                post_obj.sport = sport
                                build_score(element.id, element.Joueur1_ID, function (score_j1) {
                                    post_obj.score_j1 = score_j1
                                    build_score(element.id, element.Joueur2_ID, function (score_j2) {
                                        post_obj.score_j2 = score_j2
                                        post_obj.matchNom = j1.Nom + " - " + j2.Nom
                                        arr.push(post_obj);
                                        callback(arr)
                                    })
                                })

                            })
                        })
                    })
                })


            });

        }
    });




    /*  
      o.id = rows[i].id
                          o.etat = rows[i].etat
                          o.DateTime = rows[i].DateTime
     
    */


    app.get('/Partie/:id', function (req, res1) {
        process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
        mysql.query("select id, OrganisationID, Arbitre_ID, Joueur1_ID, Joueur2_ID, SportId, DateTime, etat from Partie where id=" + req.params.id + ";", function (err, rows) {
            if (!err) {
                var o = {} // empty Object
                o.id = rows[0].id
                o.etat = rows[0].etat
                o.DateTime = rows[0].DateTime
                request('https://localhost/organisation/' + rows[0].OrganisationID, { json: true }, (err, res, body) => {
                    if (err) { return console.log(err); }
                    o.organisation = body
                    console.log(body);
                    request('https://localhost/arbitre/' + rows[0].Arbitre_ID, { json: true }, (err, res, body) => {
                        if (err) { return console.log(err); }
                        o.arbitre = body
                        request('https://localhost/joueur/' + rows[0].Joueur1_ID, { json: true }, (err, res, body) => {
                            if (err) { return console.log(err); }
                            o.joueur1 = body[0]
                            request('https://localhost/joueur/' + rows[0].Joueur2_ID, { json: true }, (err, res, body) => {
                                if (err) { return console.log(err); }
                                o.joueur2 = body[0]
                                request('https://localhost/sport/' + rows[0].SportId, { json: true }, (err, res, body) => {
                                    if (err) { return console.log(err); }
                                    o.sport = body

                                    res1.json(o);
                                });
                            });
                        });
                    });
                });
            } else {
                res1.json({ "code": 500, "status": err });
                return;
            }

        });
    });

    app.post('/Partie', function (req, res, err) {
        var cookie = req.cookies.jwtTokenOrga;
        jwt.verify(cookie, config.keys.organisationKey, (err, authData) => {
            if (err) {
                res.sendStatus(403);
            } else {
                console.log(req.body);
                mysql.query("INSERT INTO Partie(organisationID, arbitre_id, joueur1_id, joueur2_id, sportID, etat, DateTime) VALUES ('" + req.body.organisation + "','" + req.body.arbitre + "','" + req.body.joueur1
                    + "','" + req.body.joueur2 + "','" + req.body.sport + "','" + req.body.etat + "','" + req.body.DateTime + "');", function (err, rows) {
                        if (!err) {
                            res.json({ "code": 200 });
                        } else {
                            res.json({ "code": 500, "status": err });
                            return;
                        }
                        mysql.on('error', function (err) {
                            res.json({ "code": 100, "status": "Error in connection database" });

                            return;
                        });
                    });
            }
        });
    });

    app.put('/Partie/:id', (req, res, err) => {
        var cookie = req.cookies.jwtTokenOrga;
        jwt.verify(cookie, config.keys.organisationKey, (err, authData) => {
            if (err) {
                res.sendStatus(403);
            } else {
                mysql.query("update Partie set organisationID ='" + req.body.organisation + "', arbitre_id ='" + req.body.arbitre + "', joueur1_id ='" +
                    req.body.joueur1 + "', joueur2_id ='" + req.body.joueur2 + "', sportId ='" + req.body.sport + "', etat ='" + req.body.etat + "', datetime ='" + req.body.DateTime + "'where id =" + req.params.id + ";",
                    function (err, rows) {
                        if (!err) {
                            res.json({ "code": 200 });
                        } else {
                            res.json({ "code": 500, "status": err });
                            return;
                        }
                    });
                mysql.on('error', function (err) {
                    res.json({ "code": 100, "status": "Error in connection database" });

                    return;
                });
            }
        });
    });

    app.put('/PartieEtat/:id', (req, res, err) => {
        var cookie = req.cookies.jwtToken;
        jwt.verify(cookie, config.keys.arbitreKey, (err, authData) => {
            if (err) {
                res.sendStatus(403);
            } else {
                var sql = "update Partie set  etat ='" + req.body.etat + "' where id =" + req.params.id + ";";
                var sql2 = "update Partie set  etat ='0' where Arbitre_id =" + req.body.id_arbitre + ";";
                console.log(sql, sql2, req.params);
                mysql.query(sql2, function (err, rows) {
                    if (!err) {
                        mysql.query(sql,
                            function (err, rows) {
                                if (!err) {
                                    res.json({ "code": 200 });
                                } else {
                                    res.json({ "code": 500, "status": err });
                                    return;
                                }
                            });
                    }
                });
                mysql.on('error', function (err) {
                    res.json({ "code": 100, "status": "Error in connection database" });

                    return;
                });
            }
        })
    });


    app.delete('/Partie/:id', (req, res, err) => {
        var cookie = req.cookies.jwtTokenOrga;
        jwt.verify(cookie, config.keys.organisationKey, (err, authData) => {
            if (err) {
                res.sendStatus(403);
            } else {
                mysql.query("delete from Partie where id=" + req.params.id + ";",
                    function (err, rows) {
                        if (!err) {
                            res.json({ "code": 200 });
                        } else {
                            res.json({ "code": 500, "status": err });
                            return;
                        }
                    });
                mysql.on('error', function (err) {
                    res.json({ "code": 100, "status": "Error in connection database" });

                    return;
                });
            }
        })
    });

}

