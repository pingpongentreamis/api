

module.exports = function (app) {
    const jwt = require('jsonwebtoken');
    var env = process.env.NODE_ENV || 'production';
    var config = require('../config')[env];

    var mysql = require('../services/db').pool;
    app.get('/test1', function (req, res) {
        mysql.query("select * from Joueur", function (err, rows) {
            res.json(rows);
            console.log(rows);
        })
    });

    app.get('/verifyArbitreToken', function (req, res) {
        var cookie = req.cookies.jwtToken;
      //  console.log("Cookie yes " + cookie)
        jwt.verify(cookie, config.keys.arbitreKey, (err, authData) => {
            if (err) {
               // res.sendStatus(403);
                res.json({ 'status': '403', 'valid': 'false' })
            } else {
                res.json({ 'status': '200', 'valid': 'true' })
            }
        })
    })

    app.get('/verifyOrganisationToken', function (req, res) {
        var cookie = req.cookies.jwtTokenOrga;
     //   console.log("Cookie yes " + cookie)
        jwt.verify(cookie, config.keys.organisationKey, (err, authData) => {
            if (err) {
               // res.sendStatus(403);
                res.json({ 'status': '403', 'valid': 'false' })
            } else {
                res.json({ 'status': '200', 'valid': 'true' })
            }
        })
    })
}

