module.exports = function (app) {
    var mysql = require('../services/db').pool;
    var bcrypt = require('bcryptjs');
    const jwt = require('jsonwebtoken');
    var env = process.env.NODE_ENV || 'production';
    var config = require('../config')[env];

    app.get('/Arbitres', function (req, res) {
        mysql.query("select id, Nom, Prenom, Nationalite from Arbitre", function (err, rows) {
            if (!err) {
                res.json(rows);
            } else {
                res.json({ "code": 500, "status": err });
                return;
            }

        });
    });


    app.post("/ArbitresLogin", (req, res, err) => {
        var cookie = req.cookies.jwtToken;
        //  console.log("the cookie", req);
        const user = req.body.login
        mysql.query("select pass,id from Arbitre where Login = '" + req.body.login + "'", function (err, rows) {
            if (!err) {
                console.log("code : 200 ");
                if (rows.length != 0) {
                    passCheck = bcrypt.compareSync(req.body.pass, rows[0].pass);
                    console.log("password : " + passCheck)

                    jwt.sign({ user }, config.keys.arbitreKey, { expiresIn: 60 * 60 }, (err, token) => {
                        if (!cookie) {
                            res.cookie('jwtToken', token, { maxAge: 24 * 60 * 60 * 1000, httpOnly: true });
                        }
                        else {
                            console.log("let's check that this is a valid cookie");
                            jwt.verify(cookie, config.keys.arbitreKey, (err, authData) => {
                                if (err) {
                                    res.cookie('jwtToken', token, { maxAge: 24 * 60 * 60 * 1000, httpOnly: true });
                                } else {
                                    console.log("valid cookie")
                                }
                            })
                        }
                        res.json({ "code": 200, "password": passCheck, "login": true, "idArbitre": rows[0].id });
                    });
                } else {
                    res.json({ "code": 200, "password": false, "login": false, "statut": "wrong login" });
                }

            } else {
                res.json({ "code": 500, "status": err });
                console.log("error")
                return;
            }
        });
        console.log(req.body.mail)

        mysql.on('error', function (err) {
            res.json({ "code": 100, "status": "Error in connection database" });

            return;
        });
    });


    app.get('/Arbitre/:id', function (req, res) {
        mysql.query("select id, Nom, Prenom, Nationalite from Arbitre where id=" + req.params.id + ";", function (err, rows) {
            if (!err) {
                res.json(rows);
            } else {
                res.json({ "code": 500, "status": err });
                return;
            }
            /* */
        });
    });

    app.post('/Arbitre', function (req, res, err) {
        var cookie = req.cookies.jwtTokenOrga;
        console.log("Cookie yes " + req)
        jwt.verify(cookie, config.keys.organisationKey, (err, authData) => {
            if (err) {
                res.sendStatus(403);
            } else {
                console.log(req.body);
                bcrypt.hash(req.body.pass, 10, function (err, hash) {
                    console.log(hash);
                    if (hash === undefined || hash === '') {
                        res.json({ "code": 500, "status": "Error in pass" });
                    }
                    mysql.query("INSERT INTO Arbitre(nom, prenom, nationalite, login, pass) VALUES ('" + req.body.nom_arbitre + "','" + req.body.prenom_arbitre + "','" + req.body.nationalite_arbitre
                        + "','" + req.body.login + "','" + hash + "');", function (err, rows) {
                            if (!err) {
                                res.json({ "code": 200 });
                            } else {
                                res.json({ "code": 500, "status": err });
                                return;
                            }
                            mysql.on('error', function (err) {
                                res.json({ "code": 100, "status": "Error in connection database" });

                                return;
                            });
                        });
                });
            }
        })
    });

    app.put('/Arbitre/:id', (req, res, err) => {
        mysql.query("update Arbitre  set nom ='" + req.body.nom_arbitre + "', prenom ='" + req.body.prenom_arbitre + "', nationalite ='" +
            req.body.nationalite_arbitre + "', login ='" + req.body.login + "'where id =" + req.params.id + ";",
            function (err, rows) {
                if (!err) {
                    res.json({ "code": 200 });
                } else {
                    res.json({ "code": 500, "status": err });
                    return;
                }

                mysql.on('error', function (err) {
                    res.json({ "code": 100, "status": "Error in connection database" });

                    return;
                });
            });
    });


    app.put('/ArbitreMdp/:id', (req, res, err) => {
        bcrypt.hash(req.body.pass, 10, function (err, hash) {
            mysql.query("update Arbitre set pass='" + hash + "' where id =" + req.params.id, function (err, rows) {
                if (!err) {
                    res.json({ "code": 200 });
                } else {
                    res.json({ "code": 500, "status": err });
                    return;
                }
                mysql.on('error', function (err) {
                    res.json({ "code": 100, "status": "Error in connection database" });

                    return;
                });
            });
        });
    });



    app.delete('/Arbitre/:id', (req, res, err) => {
        mysql.query("delete from Arbitre where id=" + req.params.id + ";",
            function (err, rows) {
                if (!err) {
                    res.json({ "code": 200 });
                } else {
                    res.json({ "code": 500, "status": err });
                    return;
                }
                mysql.on('error', function (err) {
                    res.json({ "code": 100, "status": "Error in connection database" });

                    return;
                });
            });

    });


    app.delete('/Arbitre/:id', (req, res, err) => {
        mysql.query("update Arbitre set nom ='unknown', prenom ='unknown', nationalite ='unknown' where id =" + req.params.id + ";",
            function (err, rows) {
                if (!err) {
                    res.json({ "code": 200 });
                } else {
                    res.json({ "code": 500, "status": err });
                    return;
                }
            });
        mysql.on('error', function (err) {
            res.json({ "code": 100, "status": "Error in connection database" });

            return;
        });
    });
}