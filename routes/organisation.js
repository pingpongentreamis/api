module.exports = function (app) {
    var mysql = require('../services/db').pool;
    var bcrypt = require('bcryptjs');
    const jwt = require('jsonwebtoken');
    var env = process.env.NODE_ENV || 'production';
    var config = require('../config')[env];

    app.get('/Organisations', function (req, res) {
        mysql.query("select id, Nom, Pays, Ville from Organisation", function (err, rows) {
            if (!err) {
                res.json(rows);
            } else {
                res.json({ "code": 500, "status": err });
                return;
            }

        });
    });

    app.post("/OrganisationLogin", (req, res, err) => {
        var cookie = req.cookies.jwtTokenOrga;
        console.log(req.body);
        const user = req.body.login
        var sql = "select pass,id from Organisation where Login = '" + req.body.login + "'";
        // console.log(sql);
        mysql.query(sql, function (err, rows) {
            if (!err) {
                var passCheck;
                console.log("code : 200 ");
                if (rows.length != 0) {
                    passCheck = bcrypt.compareSync(req.body.pass, rows[0].pass);
                    console.log("password : " + passCheck)
                    jwt.sign({ user }, config.keys.organisationKey, { expiresIn: 60 * 60 }, (err, token) => {
                        if (!cookie) {
                            res.cookie('jwtTokenOrga', token, { maxAge: 24 * 60 * 60 * 1000, httpOnly: true });
                        }
                        else {
                            console.log("let's check that this is a valid cookie");
                            jwt.verify(cookie, config.keys.organisationKey, (err, authData) => {
                                if (err) {
                                    res.cookie('jwtTokenOrga', token, { maxAge: 24 * 60 * 60 * 1000, httpOnly: true });
                                } else {
                                    console.log("valid cookie")
                                }
                            })
                        }
                        res.json({ "code": 200, "password": passCheck, "login": true, "idOrganisation": rows[0].id });
                    });
                } else {
                    res.json({ "code": 200, "password": false, "login": false, "statut": "wrong login" });
                }

            } else {
                res.json({ "code": 500, "status": err });
                console.log("error", err)
                return;
            }
        });
        console.log(req.body.mail)

        mysql.on('error', function (err) {
            res.json({ "code": 100, "status": "Error in connection database" });

            return;
        });
    });


    app.get('/Organisation/:id', function (req, res) {
        mysql.query("select id, Nom, Pays, Ville from Organisation where id=" + req.params.id + ";", function (err, rows) {
            if (!err) {
                res.json(rows);
            } else {
                res.json({ "code": 500, "status": err });
                return;
            }

        });
    });



    app.post('/Organisation', function (req, res, err) {
        console.log(req.body);
        bcrypt.hash(req.body.pass, 10, function (err, hash) {
            console.log(hash);
            if (hash === undefined || hash === '') {
                res.json({ "code": 500, "status": "Error in pass" });
            }
            mysql.query("INSERT INTO Organisation(nom, login, pass, ville, pays) VALUES ('" + req.body.nom + "','" + req.body.login + "','" + hash + "','" + req.body.ville
                + "','" + req.body.pays + "');", function (err, rows) {
                    if (!err) {
                        res.json({ "code": 200 });
                    } else {
                        res.json({ "code": 500, "status": err });
                        return;
                    }
                    mysql.on('error', function (err) {
                        res.json({ "code": 100, "status": "Error in connection database" });

                        return;
                    });
                });
        });
    });

}


